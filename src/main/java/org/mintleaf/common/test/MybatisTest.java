package org.mintleaf.common.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mintleaf.modules.sys.dao.SysGeneratorMapper;
import org.mintleaf.modules.sys.entity.Category;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.mintleaf.modules.sys.dao.CategoryMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class MybatisTest {

	@Autowired
	private CategoryMapper categoryMapper;

	@Autowired
	SysGeneratorMapper sysGeneratorMapper;

	@Test
	public void testList() {
		PageHelper.offsetPage(0, 5);
		List<Category> cs=categoryMapper.list();
		System.out.println(cs.getClass());
		for (Category c : cs) {
			System.out.println(c.getName());
		}
		System.out.println(new PageInfo(cs).getTotal());
	}

	@Test
	public void testQueryList() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("order", "desc");
		map.put("sidx", "id");
//		map.put("id", "12345677");


//		PageHelper.startPage(1, 3);
		List<Category> cs=categoryMapper.queryList(map);
		System.out.println(cs.getClass());
		for (Category c : cs) {
			System.out.println(c.getName());
		}
//		System.out.println(new PageInfo(cs).getTotal());
	}

	@Test
	public void testGenList() {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Map<String, Object>> cs = sysGeneratorMapper.queryList(map);
		System.out.println(cs.getClass());
		for (Map c : cs) {
			System.out.println(c);
		}

	}

}
