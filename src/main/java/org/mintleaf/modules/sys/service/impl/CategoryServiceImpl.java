package org.mintleaf.modules.sys.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.mintleaf.modules.sys.dao.CategoryMapper;
import org.mintleaf.modules.sys.entity.Category;
import org.mintleaf.modules.sys.service.CategoryService;

@Service
public class CategoryServiceImpl  implements CategoryService{

	@Autowired
    CategoryMapper categoryMapper;
	
	
	public List<Category> list(){
		return categoryMapper.list();
	}

	public String add(Category category){
		int result=0;
		String resultStr="添加失败";
		result=categoryMapper.add(category);
//		try{
//			result=categoryMapper.add(category);
//
//		}catch (Exception e){
//			resultStr="添加失败:"+e.getMessage();
//		}
		if(result==1){
			return "添加成功";
		}else {
			return resultStr;
		}
	}

	public String delete(int id){
		int result=0;
		String resultStr="删除失败";
		result=categoryMapper.delete(id);
		if(result==1){
			return "删除成功";
		}else {
			return resultStr;
		}
	}


	public void deleteBatch(Long[] id) {

		categoryMapper.deleteBatch(id);
	}

	public String update(Category category){
		int result=0;
		String resultStr="更新失败";
		result=categoryMapper.update(category);
		if(result==1){
			return "更新成功";
		}else {
			return resultStr;
		}
	}
	public Category get(int id){

		Category result=categoryMapper.get(id);
		return result;

	}
	public List<Category> listwhere(Category category){
		return categoryMapper.listwhere(category);
	}

	public List<Category> queryList(Map<String, Object> map){

		return categoryMapper.queryList(map);

	}

}
