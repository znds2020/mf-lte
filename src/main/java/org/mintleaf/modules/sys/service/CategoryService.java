package org.mintleaf.modules.sys.service;

import org.mintleaf.modules.sys.entity.Category;

import java.util.List;
import java.util.Map;

public interface CategoryService {

	List<Category> list();
	String add(Category category);
	String delete(int id);
	void deleteBatch(Long[] id);
	String update(Category category);
	Category get(int id);
	List<Category> listwhere(Category category);
	List<Category> queryList(Map<String, Object> map);
}
