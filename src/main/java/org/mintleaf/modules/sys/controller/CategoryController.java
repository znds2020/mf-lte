package org.mintleaf.modules.sys.controller;


import java.util.List;
import java.util.Map;
import java.util.Random;

import org.mintleaf.common.util.PageUtils;
import org.mintleaf.common.util.Query;
import org.mintleaf.common.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.mintleaf.modules.sys.entity.Category;
import org.mintleaf.modules.sys.service.CategoryService;
import org.mintleaf.common.util.Page;

//restful风格控制器
@RestController
@RequestMapping("")
public class CategoryController {
	@Autowired
	CategoryService categoryService;

	@RequestMapping("main")
	public ModelAndView main(){
		String url = "redirect:main.jsp";
		return new ModelAndView(url);
	}

	@RequestMapping("login")
	public ModelAndView login(){
		String url = "redirect:login.jsp";
		return new ModelAndView(url);
	}

	@RequestMapping("generator")
	public ModelAndView generator(){
		ModelAndView view =new ModelAndView();
		view.setViewName("sys/generator");
		return view;
	}

	@RequestMapping("category")
	public ModelAndView Category(){
		ModelAndView view =new ModelAndView();
		view.setViewName("sys/category");
		return view;
	}

	@RequestMapping("icon")
	public ModelAndView icon(){

		String url = "redirect:http://www.fontawesome.com.cn/faicons/";
		return new ModelAndView(url);
	}




    //分页查询
    @RequestMapping("pageCategory")
    public R pageCategory(@RequestParam Map<String, Object> params){
    	Query query = new Query(params);
	    PageHelper.startPage(query.getPage(),query.getLimit());
	    List<Category> cs= categoryService.queryList(query);
	    int total = (int) new PageInfo<>(cs).getTotal();
	    PageUtils pageUtil = new PageUtils(cs, total, query.getLimit(), query.getPage());
	    return R.ok().put("page", pageUtil);

    }

    //添加数据方法
    @RequestMapping(value = "addCategore",method = {RequestMethod.POST}) //请求类型
    public R addCategore(@RequestBody Category category){
        category.setId(new Random().nextInt()); //生成随机数
        String result= categoryService.add(category);
        return R.ok();
    }

    //删除数据方法
    @RequestMapping(value ="/delete",method = {RequestMethod.POST})
    public R delete(@RequestBody Long[] Ids){
        categoryService.deleteBatch(Ids);
        return R.ok();
    }

    //更新数据方法
    @RequestMapping(value = "updateCategore",method = {RequestMethod.POST})
    public R updateCategore(@RequestBody Category category){
        String result= categoryService.update(category);
        return R.ok();
    }

    //查询一条数据
    @RequestMapping(value = "getCategore",method = {RequestMethod.GET})
    public R getCategore(int id){
        Category category=new Category();
        category.setId(id);
        Category result= categoryService.get(id);
        return R.ok().put("category", result);

    }


    //老代码
    @RequestMapping("listCategory")
    public ModelAndView listCategory(Page page){
        ModelAndView mav = new ModelAndView();
        PageHelper.offsetPage(page.getStart(),5);
        List<Category> cs= categoryService.list();
        int total = (int) new PageInfo<>(cs).getTotal();

        page.caculateLast(total);

        // 放入转发参数
        mav.addObject("cs", cs);
        // 放入jsp路径
        mav.setViewName("listCategory");
        return mav;
    }

    // 查询全部
	@RequestMapping(value = "getCategories")
	public List<Category> getCategories(){
		List<Category> cs= categoryService.list();
		return cs;
	}

	//删除数据方法
	@RequestMapping(value = "deleteCategore",method = {RequestMethod.GET})
	public String deleteCategore(int id){
		Category category=new Category();
		category.setId(id);
		String result= categoryService.delete(id);
		return result;
	}

	//条件查询数据
	@RequestMapping(value = "getwhereCategories")
	public List<Category> getwhereCategories(String name){
		Category category=new Category();
		category.setName(name);
		List<Category> cs= categoryService.listwhere(category);
		return cs;
	}
}
