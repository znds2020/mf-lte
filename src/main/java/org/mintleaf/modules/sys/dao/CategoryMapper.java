package org.mintleaf.modules.sys.dao;
 
import java.util.List;
import java.util.Map;

import org.mintleaf.modules.sys.entity.Category;

public interface CategoryMapper {
 
      
    public int add(Category category);
       
      
    public int delete(int id);

    public void deleteBatch(Long[] id);

       
      
    public Category get(int id);  
     
      
    public int update(Category category);   
       
      
    public List<Category> list();

    public List<Category> listwhere(Category category);

    public List<Category> queryList(Map<String, Object> map);
      
}