package org.mintleaf.modules.sys.dao;

import org.mintleaf.modules.sys.entity.TestStart;

public interface TestStartMapper {
    int deleteByPrimaryKey(String id);

    int insert(TestStart record);

    int insertSelective(TestStart record);

    TestStart selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(TestStart record);

    int updateByPrimaryKey(TestStart record);
}