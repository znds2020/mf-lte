package org.mintleaf.modules.sys.entity;

public class TestStart {
    private String id;

    private String url;

    private String name;

    public TestStart(String id, String url, String name) {
        this.id = id;
        this.url = url;
        this.name = name;
    }

    public TestStart() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }
}