<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" import="java.util.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>了解MF-LTE</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/AdminLTE.min.css">
    <link rel="stylesheet" href="css/all-skins.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
</head>
<body >
<div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>150</h3>

                <p>购买</p>
            </div>
            <div class="icon">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <a href="#" class="small-box-footer">
                更多信息 <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3>1000</h3>

                <p>访问</p>
            </div>
            <div class="icon">
                <i class="fa fa-user-o"></i>
            </div>
            <a href="#" class="small-box-footer">
                更多信息 <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>190</h3>

                <p>会话</p>
            </div>
            <div class="icon">
                <i class="fa fa-comment-o"></i>
            </div>
            <a href="#" class="small-box-footer">
                更多信息 <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3>650</h3>

                <p>收藏</p>
            </div>
            <div class="icon">
                <i class="fa fa-heart-o"></i>
            </div>
            <a href="#" class="small-box-footer">
                更多信息 <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->
</div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">基本信息</h3>
            <div class="box-tools pull-right">
                <!-- Buttons, labels, and many other things can be placed here! -->
                <!-- Here is a label for example -->

            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <ul>
                <li>MF-LTE，即MintleafFrameworkLTE，基于SSM架构的轻量级开发框架，框架其实就是某种应用的半成品，把不同应用程序中有共性的一些东西抽取出来，做成一个半成品程序，这样的半成品就是所谓的程序框架。搭建本项目主要做学习研究之用。基于velocity模板引擎编写了代码生成工具，根据数据库表结构生成sql、dao、manager、service、controller、html、js基础代码，更快的开发方式。命名规范和工程分层规约参考阿里巴巴JAVA开发规范</li>
                <li>Git地址：<a href="https://gitee.com/ZhangMengchu/mf-lte" target="_blank">https://gitee.com/ZhangMengchu/mf-lte</a></li>
                <li>如需关注项目最新动态，请Watch、Star项目，同时也是对项目最好的支持</li>
            </ul>
        </div>
     <%--<div class="box-footer">--%>
       <%--</div>--%>
    </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">技术方案</h3>
                <div class="box-tools pull-right">
                    <!-- Buttons, labels, and many other things can be placed here! -->
                    <!-- Here is a label for example -->

                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <ul>
                    <li>核心框架：Spring 4.1.3</li>
                    <li>WEB框架：SpringMVC 4.1.3</li>
                    <li>ORM框架：MyBatis 3.1.1</li>
                    <li>项目管理：Maven 3.0</li>
                    <li>模板引擎：JSP、Velocity 1.7</li>
                    <li>JS框架：Vue.js 2.3.3</li>
                    <li>主页框架：AdminLTE(Bootstrap) 2.3.7</li>
                    <li>表格插件：JqueryGrid 5.1.1</li>
                    <li>弹窗组件：Layer 3.0.1</li>

                </ul>
            </div>
            <%--<div class="box-footer">--%>
            <%--</div>--%>
        </div>
    </div>
</div>
</body>
</html>