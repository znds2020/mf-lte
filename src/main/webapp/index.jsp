<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" import="java.util.*"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>MintLeafFramework</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="shortcut icon" href="images/mintleaf.png" />
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="css/all-skins.min.css">
    <link rel="stylesheet" href="css/main.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!-- ADD THE CLASS layout-boxed TO GET A BOXED LAYOUT -->
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper" id="rrapp" v-cloak>
    <header class="main-header">
        <a href="javascript:void(0);" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b><i class="fa fa-leaf"></i></b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>MF-LTE</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="hidden-xs" style="float:left;color:white;padding:15px 10px;">遵循从开源中来到开源去的精神原则和结合自身对技术的理解和实践，从而获得更加完善的解决方案。</div>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li><a href="javascript:;" @click="donate"><i class="fa fa-jpy"></i> &nbsp;捐赠作者</a></li>
                    <li><a href="http://39.106.153.65:8088/MintLeafNetCompany/html/index.html" target="_blank"><i class="fa fa-home"></i> &nbsp;MintLeaf</a></li>
                    <li><a href="http://mf-dev.mydoc.io/" target="_blank"><i class="fa fa-file-text-o"></i> &nbsp;项目文档</a></li>
                </ul>
            </div>
        </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">导航菜单</li>
                <li class="active">
                    <a href="javascript:;"><i class="fa fa-home"></i><span>主页</span><i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu menu-open" style="display: block;">
                        <li class="active"><a href="#main"><i class="fa fa-info-circle"></i>了解MF-LTE</a></li>
                    </ul>
                </li>
                <li><a href="#category"><i class="fa fa-table"></i><span>CURD演示</span></a></li>
                <li><a href="#generator"><i class="fa fa-code"></i><span>代码生成器</span></a></li>
                <li><a href="#login"><i class="fa fa-user"></i><span>登陆页面</span></a></li>
                <li><a href="#icon"><i class="fa fa-font-awesome"></i><span>图标库</span></a></li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-share"></i> <span>多级菜单</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-circle-o"></i> 一级</a></li>
                        <li class="treeview">
                            <a href="#"><i class="fa fa-circle-o"></i> 一级
                                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="#"><i class="fa fa-circle-o"></i> 二级</a></li>
                                <li class="treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i> 二级
                                        <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li><a href="#test"><i class="fa fa-circle-o"></i> 三级(测试路由)</a></li>
                                        <li><a href="#"><i class="fa fa-circle-o"></i> 三级</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="#"><i class="fa fa-circle-o"></i> 一级</a></li>
                    </ul>
                </li>

            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
    <!-- =============================================== -->
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <ol class="breadcrumb" id="nav_title" style="position:static;float:none;">
                <li class="active">
                    <i class="" style="font-size:20px;position:relative;top:2px;left:-3px;"></i> &nbsp; 菜单</li>
                <li class="active">{{navTitle}}</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content" style="background: #ffffff;">
            <iframe scrolling="yes" frameborder="0" style="width:100%;min-height:200px;overflow:visible;background:#fff;" :src="main"></iframe>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            Version 1.0.0
        </div>
        2019 &copy; Powered by <a href="http://39.106.153.65:8088/MintLeafNetCompany/html/index.html" style="color: #01AAED;" target="_blank">MintLeaf</a>
    </footer>

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>



</div>
<!-- ./wrapper -->

<script src="libs/jquery.min.js"></script>
<script src="libs/vue.min.js"></script>
<script src="libs/router.js"></script>
<script src="libs/bootstrap.min.js"></script>
<script src="libs/fastclick.min.js"></script>
<script src="libs/app.js"></script>
<script src="plugins/layer/layer.js"></script>
<script src="js/index.js"></script>
</body>
</html>
