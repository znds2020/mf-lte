<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*"%>
 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/AdminLTE.min.css">
<body>

				<table class="table table-bordered">
					<colgroup>
						<col width="150">
						<col width="150">
						<col>
					</colgroup>
					<thead>
					<tr>
						<th>ID</th>
						<th>名称</th>
					</tr>
					</thead>
					<tbody>
					<c:forEach items="${cs}" var="c" varStatus="st">
						<tr>
							<td>${c.id}</td>
							<td>${c.name}</td>
						</tr>
					</c:forEach>
					</tbody>
				</table>

			<div style="text-align:center">
				<a href="?start=0">首  页</a>
				<a href="?start=${page.start-page.count}">上一页</a>
				<a href="?start=${page.start+page.count}">下一页</a>
				<a href="?start=${page.last}">末  页</a>
			</div>



</body>



