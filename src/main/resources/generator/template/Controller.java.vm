package ${package}.${moduleName}.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ${package}.${moduleName}.entity.${className}Entity;
import ${package}.${moduleName}.service.${className}Service;
import ${mainPath}.util.PageUtils;
import ${mainPath}.util.R;

/**
 * ${comments}
 *
 * @author ${author}
 * @email ${email}
 * @date ${datetime}
 */
@RestController
@RequestMapping("${moduleName}/${pathName}")
public class ${className}Controller extends AbstractController {

    @Autowired
    private ${className}Service ${classname}Service;
	
	/**
	 * 列表
	 * @param params
	 * @return
	 */
	@RequestMapping("/list")
	public Page<${className}Entity> list(@RequestBody Map<String, Object> params) {
		return ${classname}Service.list${className}(params);
	}
		
	/**
	 * 新增
	 * @param ${classname}
	 * @return
	 */
	@SysLog("新增${comments}")
	@RequestMapping("/save")
	public R save(@RequestBody ${className}Entity ${classname}) {
		return ${classname}Service.save${className}(${classname});
	}
	
	/**
	 * 根据id查询详情
	 * @param id
	 * @return
	 */
	@RequestMapping("/info")
	public R getById(@RequestBody Long id) {
		return ${classname}Service.get${className}ById(id);
	}
	
	/**
	 * 修改
	 * @param ${classname}
	 * @return
	 */
	@SysLog("修改${comments}")
	@RequestMapping("/update")
	public R update(@RequestBody ${className}Entity ${classname}) {
		return ${classname}Service.update${className}(${classname});
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@SysLog("删除${comments}")
	@RequestMapping("/remove")
	public R batchRemove(@RequestBody Long[] id) {
		return ${classname}Service.batchRemove(id);
	}
	
}
