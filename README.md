# MF-LTE

#### 项目介绍
MF-LTE，即MintleafFrameworkLTE，基于SSM架构的轻量级开发框架，框架其实就是某种应用的半成品，把不同应用程序中有共性的一些东西抽取出来，做成一个半成品程序，这样的半成品就是所谓的程序框架。搭建本项目主要做学习研究之用。基于velocity模板引擎编写了代码生成工具，根据数据库表结构生成sql、dao、manager、service、controller、html、js基础代码，更快的开发方式。命名规范和工程分层规约参考阿里巴巴JAVA开发规范。

#### 技术选型
- 核心框架：Spring 4.1.3
- WEB框架：SpringMVC 4.1.3
- ORM框架：MyBatis 3.1.1
- 项目管理：Maven 3.0
- 模板引擎：JSP、Velocity 1.7
- JS框架：Vue.js 2.3.3
- 主页框架：AdminLTE(Bootstrap) 2.3.7
- 表格插件：JqueryGrid 5.1.1
- 弹窗组件：Layer 3.0.1

#### 项目文档
- [开发手册](http://mf-dev.mydoc.io/)

#### 项目结构
- java：后端代码，标准MVC分层
- webapp：前端代码

#### 安装教程

1. 通过git下载源码
2. 通过IDEA、Eclipse导入项目
3. 修改applicationContext中的数据库链接配置
4. 执行Maven命令 mvn tomcat7:run 运行项目
3. 访问路径：http://localhost:8028/mf/

#### 命名规范（参考阿里巴巴Java开发手册）
-  获取单个对象的方法用 get 做前缀
-  获取多个对象的方法用 list 做前缀
-  获取统计值的方法用 count 做前缀
-  插入的方法用 save(推荐) 或 insert 做前缀
-  删除的方法用 remove(推荐) 或 delete 做前缀
-  修改的方法用 update 做前缀

#### 应用分层（参考阿里巴巴Java开发手册）
![image](src/main/webapp/images/sa.png)

#### 逻辑结构说明

Model（模型）层：pojo
1. 创建model:Category
DAO（数据存取对象）层：mapper
1. 创建Dao接口：CategoryMapper
2. 实现Dao接口：Category.xml
Controller（控制器）层：
1. 建立Controller：CategoryController
Service（服务）层：
1. 创建service：CategoryService
2. 实现service：CategoryServiceImpl
Util（工具）：工具类
Test（测试）：测试类

#### 用一句话说明白Spring那点事儿

Spring 是一个“引擎”，Spring MVC 是基于Spring的一个 MVC 框架 ，Spring Boot 是基于Spring4的条件注册的一套快速开发整合包。

#### MVC思路

1. 首先浏览器上访问路径 /listCategory
2. tomcat根据web.xml上的配置信息，拦截到了/listCategory，并将其交由DispatcherServlet处理。
3. DispatcherServlet 根据springMVC的配置，将这次请求交由CategoryController类进行处理，所以需要进行这个类的实例化
4. 在实例化CategoryController的时候，注入CategoryServiceImpl。 (自动装配实现了CategoryService接口的的实例，只有CategoryServiceImpl实现了CategoryService接口，所以就会注入CategoryServiceImpl)
5. 在实例化CategoryServiceImpl的时候，又注入CategoryMapper
6. 根据ApplicationContext.xml中的配置信息，将CategoryMapper和Category.xml关联起来了。
7. 这样拿到了实例化好了的CategoryController,并调用listCategory方法
8. 在listCategory方法中，访问CategoryService,并获取数据，并把数据放在"cs"上，接着服务端跳转到listCategory.jsp去
9. 最后在listCategory.jsp 中显示数据

#### 许可证
- [Apache License2.0](LICENSE "Apache License2.0")
